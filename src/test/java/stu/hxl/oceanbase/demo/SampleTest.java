package stu.hxl.oceanbase.demo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import stu.hxl.oceanbasedemo1.dao.entity.User;
import stu.hxl.oceanbasedemo1.dao.mapper.UserMapper;

/**
 * @author sharloon
 * @date 2019/11/18
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testSelectOne() {

        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("age", 18); //有4个匹配结果
        wrapper.orderByDesc("id");
        wrapper.last("limit 1");// 返回第一个

        User user = userMapper.selectOne(wrapper);

        System.out.println(user.getName());

    }
}
