package stu.hxl.oceanbasedemo1.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import stu.hxl.oceanbasedemo1.dao.entity.User;
import stu.hxl.oceanbasedemo1.dao.mapper.UserMapper;
import stu.hxl.oceanbasedemo1.service.IUserService;

/**
 * @author sharloon
 * @date 2019/11/18
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


    @Override
    public IPage<User> getData(Integer currentPage, Integer pageSize) {

        Page<User> page = new Page(currentPage, pageSize, false);

        IPage iPage = this.page(page);

        return iPage;
    }
}
