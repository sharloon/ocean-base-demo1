package stu.hxl.oceanbasedemo1.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import stu.hxl.oceanbasedemo1.dao.entity.User;

/**
 * @author sharloon
 * @date 2019/11/18
 */
public interface IUserService extends IService<User> {
    IPage<User> getData(Integer currentPage, Integer pageSize);
}
