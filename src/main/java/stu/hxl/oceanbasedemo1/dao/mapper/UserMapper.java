package stu.hxl.oceanbasedemo1.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import stu.hxl.oceanbasedemo1.dao.entity.User;

/**
 * @author sharloon
 * @date 2019/11/18
 */
public interface UserMapper extends BaseMapper<User> {
}
