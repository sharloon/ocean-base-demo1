package stu.hxl.oceanbasedemo1.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author sharloon
 * @date 2019/11/18
 */
@Data
@Accessors(chain = true)
@TableName(autoResultMap = true)
@NoArgsConstructor
public class User extends Model<User> {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private String email;

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }
}
