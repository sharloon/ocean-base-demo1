package stu.hxl.oceanbasedemo1.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author sharloon
 * @date 2019/11/20
 */
/**
 * 其他信息
 */
@Data
public class OtherInfo {
    /**
     * 性别
     */
    private String sex;
    /**
     * 居住城市
     */
    private String city;

}