package stu.hxl.oceanbasedemo1.controller;

import java.sql.*;

public class MysqlDriverDemo {

    public static void main(String[] args) {
        try {
            select();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void select() throws SQLException {
        String ip = "192.168.175.51";
        String port = "2881";
        String database = "demo";
        String url = String.format("jdbc:mysql://%s:%s/%s?rewriteBatchedStatements=true", ip, port, database);
        String username = "root";
        String password = "";

        String selectSql = "select * from user;";
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, password);
            PreparedStatement ps = conn.prepareStatement(selectSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                System.out.println("name=" + rs.getString("name") +
                        ", email=" + rs.getString("email"));
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != conn) {
                conn.close();
            }
        }
    }

}