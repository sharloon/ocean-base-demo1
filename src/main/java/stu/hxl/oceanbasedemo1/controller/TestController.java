package stu.hxl.oceanbasedemo1.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stu.hxl.oceanbasedemo1.dao.entity.User;
import stu.hxl.oceanbasedemo1.service.IUserService;

/**
 * @author sharloon
 * @date 2019/11/15
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private IUserService userService;

    /**
     * 1.测试返回枚举参数(带枚举的详细信息,eg："vipType":{"type":1,"desc":"一级"})
     * 2.测试读取数据库中的json数据，映射到实体中的对象属性
     *
     * @return
     */
    @GetMapping("/1")
    public User test2(String name) {

        User user = userService.getOne(Wrappers.<User>query().lambda()
                .eq(User::getName, name).last("limit 1"));

        return user;
    }

}
