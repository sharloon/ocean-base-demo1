package stu.hxl.oceanbasedemo1;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("stu.hxl.oceanbasedemo1.dao.mapper")
public class OceanBaseDemo1Application {

	public static void main(String[] args) {
		SpringApplication.run(OceanBaseDemo1Application.class, args);
	}

}
