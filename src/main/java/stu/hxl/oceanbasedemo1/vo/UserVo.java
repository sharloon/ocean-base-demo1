package stu.hxl.oceanbasedemo1.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import stu.hxl.oceanbasedemo1.dao.entity.OtherInfo;
import stu.hxl.oceanbasedemo1.enums.UserTypeEnum;
import stu.hxl.oceanbasedemo1.enums.VipTypeEnum;

import java.util.List;

/**
 * @author sharloon
 * @date 2019/11/19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserVo {

    private Long id;

    private String name;

    private Integer age;

    private String email;

    private UserTypeEnum userType;

    private VipTypeEnum vipType;

    private List<OtherInfo> otherInfo;

}
