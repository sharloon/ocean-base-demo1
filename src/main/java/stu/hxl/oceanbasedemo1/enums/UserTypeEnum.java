package stu.hxl.oceanbasedemo1.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author sharloon
 * @date 2019/11/18
 */
@Getter
@AllArgsConstructor
public enum UserTypeEnum {

    A(1, "A类"),
    B(2, "B类"),
    C(3, "C类"),
    D(4, "D类");


    private Integer type;

    private String desc;
}
