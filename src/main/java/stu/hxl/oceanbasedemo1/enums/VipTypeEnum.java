package stu.hxl.oceanbasedemo1.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * VIP类型枚举
 *
 * @author sharloon
 * @date 2019/11/18
 */
@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum VipTypeEnum {

    LEVEL_1(1, "一级"),
    LEVEL_2(2, "二级"),
    LEVEL_3(3, "三级"),
    LEVEL_4(4, "四级"),
    LEVEL_5(5, "五级");


    @EnumValue//标记数据库存的值是type
    private Integer type;

    private String desc;

}
