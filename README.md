# ocean-base-demo1

#### 介绍
OceanBase demo程序


#### 安装教程

1.  安装docker
2.  docker安装OceanBase: https://www.oceanbase.com/docs  --> OB小密机器人  --> 使用 Docker 镜像在 Linux 平台上部署 OceanBase 试用版

#### 使用说明

##### springboot+mybatis方式
1.  修改application.yml中的数据库连接
2.  启动ocean-base-demo1
3.  访问接口: http://localhost:8080/test/1?name=Jone


##### 原生驱动包启动
1.  修改application.yml中的数据库连接
2.  运行OceanDriverDemo.main()   或者  MysqlDriverDemo.main()
